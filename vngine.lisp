(defpackage #:vactions
  (:use #:cl #:dialog-actions)
  (:export #:discard
           #:textbox-size
           #:textbox-pos
           #:*storybox*
           #:*textboxes*
           #:*reverse*
           #:add-textbox
           #:next
           #:show
           #:set-background
           #:set-image
           #:clear-text
           #:set-owner
           #:unset-owner))

(defpackage #:vngine
  (:use #:cl #:petri-parser #:petri-net #:vec #:alexandria #:split-sequence #:dialog-actions #:dialog-parser #:vactions)
  (:shadow #:if)
  (:export
   #:start))
(in-package #:vngine)

(declaim
 (special
  *textboxes*
  *talkers*
  *size*
  *reverse*
  *petri-file*
  *marked*
  *story*
  *text*
  *back-text*
  *allow-select*
  *storybox*
  *background-image*
  *display-pos*
  *front*
  *select-boxes*
  *enable-reverse*
  *images*
  *display-image*
  *active*
  *stop*))

(defmacro if (test then &optional else)
  `(cl:if ,test ,then ,else))

(define-setf-expander if (test then &optional else)
  (let ((value (gensym)))
    (values ()
            ()
            `(,value)
            `(if ,test
                 (setf ,then ,value)
                 ,(if else `(setf ,else ,value) value))
            `(if ,test ,then ,@(when else `(,else))))))

(defmacro prl (&rest objects)
  (let (skip)
   `(format t "~@{~a~^ ~}~%"
             ,@(mapcar (lambda (obj)
                         (cond ((symbolp obj)
                                (prog1
                                    (if skip
                                        obj
                                        `(format nil "~@{~a: ~S~}," ,(symbol-name obj) ,obj))
                                  (setf skip nil)))
                               ((consp obj)
                                (prog1
                                    (if skip
                                        obj
                                        `(format nil "~@{~a: ~S~}," ,(symbol-name (car obj)) ,obj))
                                  (setf skip nil)))
                               ((stringp obj)
                                (setf skip t)
                                obj)
                               (t (error "Unsupported type"))))
                       objects))))

(defun funcall-if (fn &rest args)
  (when fn
    (apply fn args)))

(defun load-all-images (imagedir &aux (table (make-hash-table :test #'equal)))
  (map nil (lambda (path)
             (let ((image (sdl:load-image path :alpha 1)))
               ;;(prl (sdl:surface-info image))
               (setf (gethash (pathname-name path) table)
                     image)))
       (directory (pathname (format nil "~a/*.png" imagedir))))
  table)

(defun get-image (name)
  (gethash name *images*))

(defun set-image (name old-name)
  (setf (gethash name *images*) (gethash old-name *images*)))

(defstruct textbox
  text
  owner
  click-event 
  (pos (vector-of 'fixnum 0 0))
  (size (error "No size"))
  (color sdl:*red*))

(defun add-textbox (&rest args)
  (let ((box (apply #'make-textbox args)))
    (appendf *textboxes* (list box))
    box))

(defun font-height ()
  (* (sdl:get-font-size "" :size :h) 2))

(defun draw-textbox (box)
  (with-slots (text pos size color owner) box
    (sdl:draw-box (sdl:rectangle-from-edges pos (v+ pos size))
                  :color color
                  :stroke-color sdl:*black*)
    (let ((pos (v- pos (vector-of 'fixnum 0 (font-height)))))
      (when owner
        (sdl:draw-string-solid owner pos :color sdl:*black*))
      (dolist (line text)
        (sdl:draw-string-solid line
                               (incv pos (vector-of 'fixnum 0 (font-height)))
                               :color sdl:*white*)))))

(defun show (name &optional pos)
  (setf *display-image* (get-image name))
  (when pos
    (setv *display-pos* pos)))

(defun draw ()
  (sdl::clear-display sdl::*black*)
  (sdl:draw-surface *background-image*)
  (when *display-image*
    (sdl:draw-surface-at *display-image*
                         (mapv (lambda (pos0 pos)
                                 (floor (* pos0 pos)))
                               (v- *size*
                                   (vector-of 'fixnum
                                              (sdl:width *display-image*)
                                              (sdl:height *display-image*)))
                               *display-pos*)))
   (when *front*
    (dolist (box *textboxes*)
      (draw-textbox box)))
  (sdl:update-display))

(defun get-file (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
       while line
       collect line)))

(let ((direct nil))
  (defun undi () (setf direct (not direct)))
  (defun clear-text (textbox)
    (setf (textbox-text textbox) nil))
  (defun set-text (textbox new-text reverse)
    (with-slots (size text) textbox
      (if direct
          (setf text (list new-text))
          (if (not reverse)
              (let ((lines (floor (sdl:y size) (font-height))))
                (let ((place (member new-text text)))
                  (if place
                      (setf (cdr place) nil)
                      (nconcf text (list new-text))))
                (if (< lines (length text))
                    (pop text)))
              (let (rest)
                (let ((text-new
                       (remove-if-not (lambda (line)
                                        (when (eq new-text line)
                                          (setf rest t))
                                        rest)
                                      text
                                      :from-end t)))
                  ;;(prl rest text)
                  (if rest
                      (setf text text-new)
                      (push new-text text)))))))))

(defun set-owner (box name)
  (setf (textbox-owner box) name))

(defun unset-owner (box)
  (setf (textbox-owner box) nil))

(defun handle-text (text reverse)
  ;;(format t "~{~a ~}~%" text)
  (assert (eq reverse *reverse*) (reverse *reverse*) "Did not reverse")
  (call-action text)
  (unless (get-action (car text))
    (next reverse)))

#+nil
(defun set-text (textbox new-text)
  (with-slots (size text) textbox
    (let ((lines (floor (sdl:y size) (font-height))))
      (format t "lines: ~a~%" lines)
      (appendf text (list new-text))
      (if (< lines (length text))
          (pop text)))))

(defun next (&optional dir)
  (let ((change (not (eq dir *reverse*))))
    (setf *reverse* dir)
    (let ((text (if (not dir) (pop *text*) (pop *back-text*))))
      (if text
          (progn
            (push text (if dir *text* *back-text*))
            (handle-text text dir)
            #+nil
            (when (not (if (not dir) *text* *back-text*))
              (next dir)))
          (unless *select-boxes*
            (select dir))))
    (when change
      (when (and *select-boxes* dir)
        (uncall-event *active*))
      (unselect)
      (next dir))))

(defvar *next* nil)

(defun call-next (&optional dir)
  (unless *next*
    (setf *next* t)
    (next dir)
    (setf *next* nil)))

(defun delete-textbox (box)
  (deletef *textboxes* box)
  box)


(defun discard ()
  (delete-textbox *storybox*) (setf *storybox* nil))

(defun unselect ()
  (setq *marked* nil)
  (dolist (box *select-boxes*)
    (deletef *textboxes* box))
  (setf *select-boxes* nil))

(defun recompute-petri-net ()
  (show-petri-net :size *size* :active-only t)
  ;#+(or)
  (setf *background-image* (sdl:load-image "petri-net.png")))

(defun set-background (name)
  (setf *background-image* (get-image name)))

(defun call-event (id)
  (prl "Call" id)
  (call-transition id)
  (recompute-petri-net))

(defun uncall-event (id)
  (uncall-transition id)
  (recompute-petri-net))

(defun handle-selection (id reverse)
  (let ((text (get-event id reverse)))
    (if (not reverse)
        (progn
          (setf *active* id)
          (setf *back-text* (list (car text)))
          (setf *text*  (cdr text))
          (handle-text (car text) reverse))
        (progn
          (uncall-event id)
          (setf *active* id)
          (setf *text* (list (car text)))
          (setf *back-text*  (cdr text))
          (handle-text (car text) reverse)))))

(defun generate-event (id)
  (warn "Event ~a not defined. The posibility will be added to implement it yourself" id)
  (list (list ".say" "" id)))


(defun get-event (id &optional reverse)
  (let ((event
         (or (gethash id *story*)
             (setf (gethash id *story*) (generate-event id)))))
    (if reverse
        (reverse event)
        event)))

(defun stop ()
  (setf *stop* t))

(defun select (reverse)
  (when *allow-select*
    (when (and *active* (not reverse))
      (call-event *active*))
    (unselect)
    (let ((options (if reverse (list-current-transitions) (list-active-transitions))))
      ;;(prl options)
      (vector-bind (w) *size*
        (let ((w* (floor w 2))
              (x* (floor w 4))
              (y* 0)
              (h* 64)
              (h*2 96))
          (let ((size (vector-of 'fixnum w* h*))
                called)
            (dolist (id options)
              (destructuring-bind (name . text) (get-event id reverse)
                (let ((*storybox* (add-textbox :pos (vector-of 'fixnum x* (incf y* h*2))
                                               :size size
                                               :click-event (lambda (right)
                                                              (unless called
                                                                (when (eq reverse *reverse*)
                                                                  (if (eq (not right) reverse)
                                                                      (call-next (not reverse))
                                                                      (progn
                                                                        (unselect)
                                                                        (setq called t)
                                                                        (handle-selection id reverse)
                                                                        (call-next reverse))))))
                                               :color (if reverse sdl:*magenta* sdl:*blue*)))
                      (*text* (unless reverse (cons name text)))
                      (*back-text* (when reverse (cons name text))))
                  (next reverse)
                  (if *storybox*
                      (push *storybox* *select-boxes*)
                      (decf y* h*2))))))
          (case (length *select-boxes*)
            (0 (stop))
            (1
             (unselect)
             (handle-selection (car options) reverse)))
          (mouse-select (sdl:mouse-x) (sdl:mouse-y)))))))
    
(defvar *whitespaces*
  '(#\Space #\Newline #\Backspace #\Tab 
    #\Linefeed #\Page #\Return #\Rubout))

(defun load-story (filename)
  (let ((event (make-hash-table :test #'equal))
        (list (split-sequence ""
                              (mapcar (lambda (line)
                                        (string-trim *whitespaces* line))
                                      (get-file filename))
                              :test #'equal)))
    (loop for (name . value) in list
       do (setf (gethash name event) (parse-dialog (format nil "~{~a~%~}" value))))
    event))

(defvar *number* 1)

(defun resize (w h)
  (let ((new (vector-of 'fixnum w h)))
    (dolist (box *textboxes*)
      (with-slots (pos size) box
        (mapv #'* pos new)
        (mapv #'ceiling pos *size*)
        (mapv #'* size new)
        (mapv #'ceiling size *size*)))
    (sdl:window w h)
    (setq *size* new)))

(defun load-petri-net (filename)
  (load-file filename))

(defun mouse-select (x y)
  (setf *marked* nil)
  (dolist (box *textboxes*)
    (with-slots (pos size) box
      (vector-bind (x0 y0) pos
        (vector-bind (w h) size
          (when (and (< x0 x (+ x0 w))
                     (< y0 y (+ y0 h)))
            (setf *front* t)
            (push box *marked*)))))))


(defun start (&key
                (image-directory (format nil "~aimages" (ql:where-is-system :vngine)))
                (file-prefix (format nil "~atest" (ql:where-is-system :vngine)))
                petri-file
                dialog-file
                config-file
                (reversible nil)
              &aux
                (pn (or petri-file (when file-prefix (format nil "~a.pn" file-prefix))))
                (dg (or dialog-file (when file-prefix (format nil "~a.dg" file-prefix))))
                (lc (or config-file (when file-prefix (format nil "~a.lisp" file-prefix))))
                (*story* (if dg (load-story dg) (error "No dialog file specified")))
                *marked*
                (*front* t)
                (*text* (list))
                *active*
                *stop*
                *reverse*
                (*enable-reverse* reversible)
                (*allow-select* t)
                *back-text*
                *select-boxes*
                *textboxes*
                *display-image*
                *next*
                (*display-pos* (vector-of 'number 1/2 1/2))
                (*storybox*
                 (add-textbox :pos (vector-of 'fixnum 0 #.(* 196 3))
                              :size (vector-of 'fixnum 1280 196)
                              :color sdl:*red*
                              :click-event #'call-next))
                (*size* (vector-of 'fixnum 1280 768)))
  (with-new-petri-net
    (with-new-dialog-context
      (defaction ".set" (newname oldname)
        (let ((name (if *reverse* oldname newname)))
          (clear-text *storybox*)
          (if (not (string= name "*"))
              (progn
                (show name)
                (set-owner *storybox* name))
              (unset-owner *storybox*)))
        (next *reverse*))
      (defaction ".say" (name text)
        (declare (ignore name))
        (set-text *storybox* text *reverse*))
      (sdl:with-init (sdl:sdl-init-everything)
        (when pn (load-file pn))
        (let ((*images* (load-all-images image-directory))
              (*background-image* (when t
                                    (show-petri-net :size *size*)
                                    (sdl:load-image "petri-net.png"))))
          (when lc
            (let ((package *package*))
              (with-open-file (file lc)
                (loop for exp = (read file nil nil nil)
                   while exp
                   do (eval exp)
                   do (prl *package*)))
              (setf *package* package)))
          (sdl:window (aref *size* 0) (aref *size* 1) :title-caption "Game" :resizable nil)
          (format t "~a" (sdl:initialise-default-font sdl:*font-9x18*))
          (sdl::clear-display sdl::*black*)
          (setf (sdl:frame-rate) 30)
        (dolist (box *textboxes*)
          (with-slots (pos size) box
            (vector-bind (x0 y0) pos
              (vector-bind (w h) size
                (when (and (< x0 (sdl:mouse-x) (+ x0 w))
                           (< y0 (sdl:mouse-y) (+ y0 h)))
                  (setf *front* t)
                  (push box *marked*))))))
        (next)
        (sdl:with-events ()
          (:quit-event () t)
          (:video-resize-event (:w w :h h)
                               (resize w h))
          (:mouse-motion-event (:x x :y y)
                               (mouse-select x y))
          (:mouse-button-down-event (:button b)
                                        ;(prl *number*)
                                    (if *stop*
                                        (if (eq (eq b sdl:sdl-button-left) *reverse*)
                                            (progn (setq *stop* nil)
                                                   (call-next (not *reverse*)))
                                            (sdl:push-quit-event))
                                        (progn
                                          (when (eq b sdl:sdl-button-left)
                                            (setf *front* *marked*))
                                          (when (or (eq b sdl:sdl-button-left)
                                                    (and *enable-reverse*
                                                         (eq b sdl:sdl-button-right)))
                                            (dolist (box *marked*)
                                              (funcall-if
                                               (textbox-click-event box)
                                               (not (eq b sdl:sdl-button-left)))))
                                          (incf *number*))))
          (:key-down-event (:key key)
                           (case key
                             (:sdl-key-escape (stop))
                             (:sdl-key-q
                              ;;(prl "enter")
                              (resize 1280 768)
                              (sdl:resize-window 1280 768))
                             (:sdl-key-space (call-next *reverse*))))
          (:idle ()
                 ;;(run)
                 (draw))))))))
