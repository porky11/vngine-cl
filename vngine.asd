(asdf:defsystem #:vngine
  :author "Fabio Krapohl <fabio.u.krapohl@fau.de>"
  :description "A visual novel engine based on simple petri nets"
  :license "LLGPL"
  :depends-on (#:simple-petri-net #:dialog #:alexandria #:lispbuilder-sdl #:vectors)
  :serial t
  :components ((:file "vngine")))


