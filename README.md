## What is this?

This is an engine for Visual Novels based on my system `simple-petri-net`.
You need the systems `simple-petri-net` and `vectors`.
You can find both on my gitlab profile.
Currently when there are multiple possible things, they will be displayed as text boxes.
The default text is displayed in a big text box.

## How to test it?

```
* (ql:quickload :vngine)

* (vngine:start)

;;or see this test
* (vngine:start "test")

```

`start` can also be called with two arguments: The petri net file (`*.pn`) and the story file (`*.dg`)
They default to the example of `simple-petri-net`.
When you only specify one name, it will add the endings `.pn` and `.dg` to it.


# Dialog Language

## Language

There is a language for dialogs between persons.
The syntax of this language is pretty small:

```
;;this  is a comment
;;a comment lasts until the end of the line
;;newlines are only important for comments and texts, not for other syntax

Alice ;the next person, who says or does something is Alice
"Hi" ;Alice says "Hi"
Bob "Hi" ;Bob says "Hi", too
"How are you?" ;Alice says this. It's how you would think, when reading a book.
"I'm fine ;comments inside texts last until the end of the line or the end of the text 
How are you?" ;Multiline text. Bob says two things

;;same as:
Alice "Hi"
Bob "Hi"
Alice "How are you?"
Bob "I'm fine"
Bob "How are you"

;;People can also do something:
Alice sleep. ;call "sleep" with parameter "Alice"
;;the dot is important here, else the next line belongs to this sentence
Bob go home. ;call "go" with parameter "Bob" and "home"
Chris go home, sleep. ;chris does even both things.

;;you can mix actions and say:
Alice, silent "Hi", go away. ;the second comma is important, else "go" would do "away" 
;;in this case, "silent" could set something, so that saying will do something different
```
## Expansion

The language is defined in file `dialog.lisp`.
You can use the functions "parse-dialog" and "parse-dialog-file".
When parsing a dialog, it will return a list of actions.
An action is a list of strings, where the first string specifies action and the second specifies the name.
Here some examples for parsing Dialogs only.
```

Alice "Hi"
;; => ((".set" "Alice" "") (".say" "Alice" "Hi") (".set" "" "Alice"))

Alice "Hi"
Bob "Hello"
"How are you?
I'm fine?"
;; => ((".set" "Alice" "") (".say" "Alice" "Hi") (".set" "Bob" "Alice")
;;     (".say" "Bob" "Hello") (".set" "Alice" "Bob") (".say" "Alice" "How are you?")
;;     (".say" "Alice" "I'm fine?") (".set" "" "Alice"))
```

Here you see just the two action specifiers ".set" and ".say".
Only these two inbuild action specifiers start with ".", because the language does not allow you to use actions containing a ".".

Everytime someone says something, ".say" is called with the name of the talker and the said text as parameters.

Everytime the talking person changes, ".set" is called with the name of the new talker and the name of the current talker as parameters. This may simplify some things.

Actions are expanded simpler:

```
Alice do something with someone, sleep.
;; => ((".set" "Alice" "") ("do" "Alice" "something" "with" "someone")
;;     ("sleep" "Alice") (".set" "" "Alice"))
```

Just try it, if you don't understand it yet.

## Calling

First you need a context for doing all following things.
This can be with the macros `with-dialog-context context` and `with-new-dialog-context`.
Both return the context containing all new definitions

In order to call an action, you have to define it first:
```
(with-new-dialog-context
  ;;you can use `defaction` to define something
  ;;this is what ".say" could do. 
  (defaction ".say" (dir name text)
    ;;every action has to take two parameters at minimum
    ;;the name and the direction (dir) are required
    ;;the direction is t, if the dialog should be called backwards
    (format t "~a: ~a" name text))
    
  ;;now call it in normal direction:
  (call-action nil ;;direction
               ".say"
               (list "Alice" "Hi")))
```

For some more complex, still simple, example see in `dialog.lisp` the fucnction `test`.

## Reverse

When you use the right mouse button, you can reverse the story.
You can reverse everything you already played.
You can even select in which the reversed story is played.
This way you can do many things, and when you see, you took a wrong decision long before, you only have to reverse a specific decision.
There are also cases, where reverse may cause unwanted states, so be careful, when you enable reverse.




